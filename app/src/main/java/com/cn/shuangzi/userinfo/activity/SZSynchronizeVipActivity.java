package com.cn.shuangzi.userinfo.activity;

import com.cn.shuangzi.bean.VipInfo;
import com.cn.shuangzi.loginplugin.common.socialized.ShareWeChat;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import io.reactivex.Observable;


/**
 * Created by CN.
 */
public abstract class SZSynchronizeVipActivity extends SZSynchronizePayActivity {
    @Override
    public void synchronize() {
        SZRetrofitManager.getInstance().request(getVipUserObservable(), "vip", new SZCommonResponseListener(false) {
            @Override
            public void onResponseSuccess(String data) {
                List<VipInfo> vipInfoList = new Gson().fromJson(data, new TypeToken<List<VipInfo>>() {
                }.getType());
                if (SZValidatorUtil.isValidList(vipInfoList)) {
                    VipInfo vipInfoNew = vipInfoList.get(0);
                    if(vipInfoNew!=null){
                        VipInfo vipInfoOld  = getVipInfoOld();
                        if(vipInfoOld!=null){
                            if(vipInfoNew.getMemberValidityTime()>vipInfoOld.getMemberValidityTime()) {
                                synchronizeSuccess(vipInfoNew);
                            }else{
                                setPayErrorMinCountCondition();
                                synchronizeDelay();
                            }
                        }else{
                            synchronizeSuccess(vipInfoNew);
                        }
                    }else{
                        setPayErrorMinCountCondition();
                        synchronizeDelay();
                    }
                } else {
                    setPayErrorMinCountCondition();
                    synchronizeDelay();
                }
            }
            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                synchronizeDelay();
                if (errorCode != SZRetrofitManager.ERROR_NET_FAILED) {
                    setPayErrorMinCountCondition();
                }
            }
        });
    }

    public boolean isFinishActivityAfterSynchronizeSuccess(){
        return true;
    }
    public void synchronizeSuccess(VipInfo vipInfoNew){
        showSuccessView();
        synchronizeVipInfoSuccess(vipInfoNew);
        if (isFinishActivityAfterSynchronizeSuccess()) {
            startActivityDelayed(null);
        } else {
            destroyTask();
        }
    }

    @Override
    public boolean onClickUserDefineServiceContactType() {
        ShareWeChat.getInstance().openCompanyService(
                "ww319db112ce746335","https://work.weixin.qq.com/kfid/kfc539223cd934af350"
        );
        return true;
    }

    public abstract Observable<String> getVipUserObservable() ;

    public abstract void synchronizeVipInfoSuccess(VipInfo vipInfo);

    public abstract VipInfo getVipInfoOld();

}
