package com.cn.shuangzi.userinfo.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.SZManager;
import com.cn.shuangzi.bean.VipInfo;
import com.cn.shuangzi.bean.VipPriceInfo;
import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.loginplugin.common.socialized.ShareWeChat;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.userinfo.R;
import com.cn.shuangzi.userinfo.adp.VipAdp;
import com.cn.shuangzi.userinfo.adp.VipPowerAdp;
import com.cn.shuangzi.userinfo.bean.VipPowerInfo;
import com.cn.shuangzi.userinfo.common.UserManager;
import com.cn.shuangzi.util.SZDateUtil;
import com.cn.shuangzi.util.SZIRecyclerViewUtil;
import com.cn.shuangzi.util.SZImageLoader;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.cn.shuangzi.view.shape_imgview.CustomShapeImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;

/**
 * Created by CN.
 */

public abstract class SZBuyVipActivity extends SZBaseActivity implements View.OnClickListener {
    public CustomShapeImageView imgAvatar;
    public TextView txtNickname;
    public ImageView imgVip;
    public View lltVipDate;
    public TextView txtNormalUser;
    public TextView txtVipDate;
    public TextView txtBuyNotes;
    public TextView txtGetInvoice;
    public TextView txtVipPowerDesc;
    public RecyclerView recyclerViewVip;
    public RecyclerView recyclerViewVipPower;
    public SZImageLoader imageLoader;
    public List<VipPriceInfo> vipPriceInfoList;
    public List<VipPowerInfo> vipPowerInfoList;
    public VipPriceInfo vipPriceInfoCheck;
    private boolean isLogin;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_buy_vip;
    }

    @Override
    protected void onBindChildViews() {
        imgAvatar = findViewById(R.id.imgAvatar);
        txtNickname = findViewById(R.id.txtNickname);
        imgVip = findViewById(R.id.imgVip);
        txtNormalUser = findViewById(R.id.txtNormalUser);
        txtVipDate = findViewById(R.id.txtVipDate);
        recyclerViewVip = findViewById(R.id.recyclerViewVip);
        lltVipDate = findViewById(R.id.lltVipDate);
        recyclerViewVipPower = findViewById(R.id.recyclerViewVipPower);
        txtBuyNotes = findViewById(R.id.txtBuyNotes);
        txtGetInvoice = findViewById(R.id.txtGetInvoice);
        txtVipPowerDesc = findViewById(R.id.txtVipPowerDesc);
    }

    @Override
    protected void onBindChildListeners() {
    }

    @Override
    protected void onChildViewCreated() {
        isLogin = isLogin(false);
        setStatusBarColor(R.color.bgVipColor);
        setStatusBarTextColorStyleBelowBar(false);
        txtGetInvoice.setVisibility(isShowGetInvoice() ? View.VISIBLE : View.GONE);
        txtVipPowerDesc.setVisibility(isShowVipPowerDesc() ? View.VISIBLE : View.GONE);
        isShowContent(false);
        imageLoader = new SZImageLoader(this);
        setUserInfoView();
        SZIRecyclerViewUtil.setGridLinearLayoutManager(this, recyclerViewVip, 2, android.R.color.transparent, getResources().getDimensionPixelSize(R.dimen.marginBig), false, false);
        SZIRecyclerViewUtil.setGridLinearLayoutManager(this, recyclerViewVipPower, getVipRecyclerViewSpanCount() == 0 ? 3 : getVipRecyclerViewSpanCount(), android.R.color.transparent, 0, false, false);
        onReloadData(false);
        SZManager.getInstance().onUMEvent(SZConst.EVENT_BUY_VIP_PAGE_SHOW);
    }

    private void setUserInfoView() {
        if (isLogin(false)) {
            findViewById(R.id.lltUser).setVisibility(View.VISIBLE);
            findViewById(R.id.lltUnLogin).setVisibility(View.GONE);
            imageLoader.load(imgAvatar, getAvatar());
            txtNickname.setText(getNickname());
        } else {
            findViewById(R.id.lltUser).setVisibility(View.GONE);
            findViewById(R.id.lltUnLogin).setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isPrepared()) {
            setUserInfoView();
        }
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
        showBarTransparentStyle();
        if (isLogin(false)) {
            requestVipUserInfo(getVipUserObservable());
        } else {
            getVipPrice();
        }
    }

    private void getVipPrice() {

        request(SZRetrofitManager.getInstance().getSZRequest().getVipPriceV2(), new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                isShowContent(true);
                vipPriceInfoList = new Gson().fromJson(data, new TypeToken<List<VipPriceInfo>>() {
                }.getType());
                final VipAdp vipAdp = new VipAdp(vipPriceInfoList);
                vipAdp.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                        for (VipPriceInfo vipPriceInfo : vipPriceInfoList) {
                            vipPriceInfo.setCheck(false);
                        }
                        vipPriceInfoCheck = vipPriceInfoList.get(position);
                        vipPriceInfoList.get(position).setCheck(true);
                        vipAdp.notifyDataSetChanged();
                        SZManager.getInstance().onUMEvent(SZConst.EVENT_CLICK_VIP_SKU, vipPriceInfoCheck.getFpSubject());
                    }
                });
                recyclerViewVip.setAdapter(vipAdp);
                vipPowerInfoList = getVipPowerInfoList();
                VipPowerAdp vipPowerAdp = new VipPowerAdp(getActivity(), vipPowerInfoList);
                vipPowerAdp.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                        VipPowerInfo vipPowerInfo = vipPowerInfoList.get(position);
                        if (!onVipPowerItemClick(position, vipPowerInfo)) {
                            showNormalAlert(vipPowerInfo.getPowerName(), vipPowerInfo.getPowerDesc(), null);
                        }
                    }
                });
                recyclerViewVipPower.setAdapter(vipPowerAdp);
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                showErrorWithMsg(errorCode, errorMsg);
            }
        });
    }

    public void setVipUserInfoView() {
        imgVip.setVisibility(View.VISIBLE);
        txtNormalUser.setVisibility(View.GONE);
        lltVipDate.setVisibility(View.VISIBLE);
        imgAvatar.setBorderWidth(SZUtil.dip2px(3));
        imgAvatar.invalidate();
        txtNickname.setTextColor(getResources().getColor(R.color.txtVipNicknameColor));
        txtVipDate.setText(SZDateUtil.getShowYearMonthDate(new Date(getVipUserInfo().getMemberValidityTime())));
    }

    public void setNormalUserView() {
        imgVip.setVisibility(View.GONE);
        txtNormalUser.setVisibility(View.VISIBLE);
        lltVipDate.setVisibility(View.GONE);
        imgAvatar.setBorderWidth(0);
        imgAvatar.invalidate();
        txtNickname.setTextColor(getResources().getColor(android.R.color.white));
    }

    @Override
    public boolean isShowTitleInit() {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPrepared() && !isLogin) {
            if (isLogin(false)) {
                isLogin = true;
                setUserInfoView();
                onReloadData(false);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();

        if (viewId == R.id.imgBackSub) {
            finish();
        } else if (viewId == R.id.btnPay) {
            if (isLogin(true)) {
                if (vipPriceInfoCheck == null) {
                    SZToast.warning("请先点击选择VIP套餐！");
                    return;
                }
                startActivity(vipPriceInfoCheck, getPayActivity());
            }
        } else if (viewId == R.id.txtBuyNotes) {
            startActivity(getBuyNotesActivity());
        } else if (viewId == R.id.txtGetInvoice) {
            if (isLogin(true)) {
                startActivity(getInvoiceUrl(), getWebViewActivity());
            }
        } else if (viewId == R.id.txtContactService) {
            ShareWeChat.getInstance().openCompanyService(
                    "ww319db112ce746335", "https://work.weixin.qq.com/kfid/kfc539223cd934af350"
            );
        }
    }

    public String getInvoiceUrl() {
        return "https://www.xiangmaikeji.com/invoice.html?appToken=" + SZApp.getInstance().getUserToken()
                + "&applicationId=" + SZApp.getInstance().getSZAppId() + "#/OrderList";
    }

    public void requestVipUserInfo(Observable<String> observable) {
        request(observable, new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                List<VipInfo> vipInfoList = new Gson().fromJson(data, new TypeToken<List<VipInfo>>() {
                }.getType());
                if (SZValidatorUtil.isValidList(vipInfoList)) {
                    setVipUserInfo(vipInfoList.get(0));
                    UserManager.getInstance().setVipInfo(vipInfoList.get(0));
                    setVipUserInfoView();
                } else {
                    setVipUserInfo(null);
                    UserManager.getInstance().setVipInfo(null);
                    setNormalUserView();
                }
                getVipPrice();
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                showErrorWithMsg(errorCode, errorMsg);
            }
        });
    }

    public abstract int getVipRecyclerViewSpanCount();

    public abstract List<VipPowerInfo> getVipPowerInfoList();

    public abstract Observable<String> getVipUserObservable();

    public abstract boolean isShowGetInvoice();

    public abstract void setVipUserInfo(VipInfo vipInfo);

    public abstract String getAvatar();

    public abstract String getNickname();

    public abstract VipInfo getVipUserInfo();

    public abstract Class<?> getPayActivity();

    public abstract Class<?> getBuyNotesActivity();

    public abstract Class<?> getWebViewActivity();

    public abstract boolean isShowVipPowerDesc();

    /**
     * 返回true，代表不需要后续触发vip权限描述弹窗，反之则需要
     *
     * @param position
     * @param vipPowerInfo
     * @return
     */
    public boolean onVipPowerItemClick(int position, VipPowerInfo vipPowerInfo) {
        return true;
    }

}

