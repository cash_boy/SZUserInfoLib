package com.cn.shuangzi.userinfo.activity;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.bean.VipInfo;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.userinfo.R;
import com.cn.shuangzi.userinfo.bean.AccountConditionInfo;
import com.cn.shuangzi.userinfo.bean.AccountConditionInfo2;
import com.cn.shuangzi.userinfo.common.UserManager;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.view.sweet_alert.SweetAlertDialog;
import com.google.gson.Gson;

import io.reactivex.Observable;

/**
 * Created by CN.
 */

public abstract class SZCancellationAccountCondition2Activity extends SZBaseActivity implements View.OnClickListener, SZInterfaceActivity {
    public CheckBox chkBoxAgree;
    public TextView txtAgreement;
    public Button btnNext;
    public View lltVipPower;
    public AccountConditionInfo2 accountConditionInfo;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_cancellation_account_condition;
    }

    @Override
    protected void onBindChildViews() {
        chkBoxAgree = findViewById(R.id.chkBoxAgree);
        txtAgreement = findViewById(R.id.txtAgreement);
        btnNext = findViewById(R.id.btnNext);
        lltVipPower = findViewById(R.id.lltVipPower);
    }

    @Override
    protected void onBindChildListeners() {
        btnNext.setOnClickListener(this);
        txtAgreement.setOnClickListener(this);
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        lltVipPower.setVisibility(isShowVipHint() ? View.VISIBLE : View.GONE);
        showBackImgLeft(getBackImgLeft());
        setTitleTxt("注销须知");
        isShowContent(false);
        onReloadData(false);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
        showBar();
        request(getUserCancellationAccountInfoObservable(), new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                isShowContent(true);
                accountConditionInfo = new Gson().fromJson(data, AccountConditionInfo2.class);
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                showErrorWithMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txtAgreement) {
            onCancellationAccountClickAgreement();
        } else if (v.getId() == R.id.btnNext) {
            if (chkBoxAgree.isChecked()) {
                VipInfo vipInfo = accountConditionInfo.getLogicMember();
                if (vipInfo != null) {
                    if (vipInfo.getMemberValidityTime() > System.currentTimeMillis()) {
                        showAlert(SweetAlertDialog.WARNING_TYPE, null, "您的帐户中还有未到期的会员，如果点击同意，" +
                                "会员将作废且不可找回，是否继续注销？", "同意", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                next();
                            }
                        }, "放弃注销", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                finish();
                            }
                        }, true);
                        return;
                    }
                }
                next();
            } else {
                SZToast.warning("请先阅读并同意注销协议！");
            }
        }
    }

    private void next() {
        boolean isDeal = false;
        if (accountConditionInfo.isWeChat2Platform()) {
            isDeal = true;
            startActivity(UserManager.PLATFORM_WECHAT_2, getCancellationAccountWechatActivityClass());
        } else if (accountConditionInfo.isWeChatPlatform()&&!isWeChat2LoginMode()) {
            isDeal = true;
            startActivity(UserManager.PLATFORM_WECHAT, getCancellationAccountWechatActivityClass());
        } else if (accountConditionInfo.isAlipayPlatform()) {
            isDeal = true;
            startActivity(UserManager.PLATFORM_ALIPAY, getCancellationAccountAlipayActivityClass());
        } else if (accountConditionInfo.isPhonePlatform()) {
            isDeal = true;
            startActivity(UserManager.PLATFORM_PHONE, getCancellationAccountPhoneActivityClass());
        }
        if(!isDeal){
            showWarningAlert("无法注销", "您目前没有绑定任何可用的帐号信息，例如微信、手机号等，可在帐号与安全功能中，绑定后再进行操作！", new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismissWithAnimation();
                    finish();
                }
            });
            return;
        }
        finish();
    }

    public abstract boolean isWeChat2LoginMode();

    public abstract boolean isShowVipHint();

    public abstract void onCancellationAccountClickAgreement();

    public abstract Class getCancellationAccountPhoneActivityClass();

    public abstract Class getCancellationAccountWechatActivityClass();

    public abstract Class getCancellationAccountAlipayActivityClass();

    public abstract Observable<String> getUserCancellationAccountInfoObservable();
}
