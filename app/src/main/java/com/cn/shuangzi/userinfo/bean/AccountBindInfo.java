package com.cn.shuangzi.userinfo.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */
public class AccountBindInfo implements Serializable {
    private String platformTypeName;
    private String boundName;
    private boolean isBound;

    public AccountBindInfo(String boundName, boolean isBound) {
        this.boundName = boundName;
        this.isBound = isBound;
    }

    public AccountBindInfo(boolean isBound) {
        this.isBound = isBound;
    }

    public AccountBindInfo() {
    }

    public String getPlatformTypeName() {
        return platformTypeName;
    }

    public String getBoundName() {
        return boundName;
    }

    public boolean isBound() {
        return isBound;
    }

    public void setPlatformTypeName(String platformTypeName) {
        this.platformTypeName = platformTypeName;
    }

    public void setBoundName(String boundName) {
        this.boundName = boundName;
    }

    public void setBound(boolean bound) {
        isBound = bound;
    }

    @Override
    public String toString() {
        return "AccountBindInfo{" +
                "platformTypeName='" + platformTypeName + '\'' +
                ", boundName='" + boundName + '\'' +
                ", isBound=" + isBound +
                '}';
    }
}
