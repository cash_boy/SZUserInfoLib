package com.cn.shuangzi.userinfo.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.SZManager;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.userinfo.R;
import com.cn.shuangzi.userinfo.bean.BindPhoneConditionInfo;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.cn.shuangzi.view.ClearEditText;

/**
 * Created by CN.
 */
public abstract class SZBaseBindPhoneStep1Activity extends SZBaseActivity implements SZInterfaceActivity {
    public ClearEditText edtPhone;
    public Button btnNext;
    public TextView txtPhoneDesc;
    public String oldPhone;
    @Override
    protected int onGetChildView() {
        return R.layout.activity_bind_phone_step1;
    }

    @Override
    protected void onBindChildViews() {
        txtPhoneDesc = findViewById(R.id.txtPhoneDesc);
        edtPhone = findViewById(R.id.edtPhone);
        btnNext = findViewById(R.id.btnNext);
    }

    @Override
    protected void onBindChildListeners() {
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPhone();
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        showBackImgLeft(getBackImgLeft());
        if(isChangeMode()) {
            setTitleTxt("更换手机");
            txtPhoneDesc.setText("请输入新的手机号");
            oldPhone = getStringExtra();
            if(oldPhone == null){
                finish();
            }
        }else{
            setTitleTxt("绑定手机");
            txtPhoneDesc.setText("请输入要绑定的手机号");
        }
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }
    public void checkPhone(){
        final String phone = edtPhone.getText().toString().trim();
        if(TextUtils.isEmpty(phone)){
            SZToast.warning("请输入手机号！");
            return;
        }

        if(!SZUtil.isValidPhoneWithTestPhone(phone)){
            SZToast.warning("请输入正确的手机号！");
            return;
        }
        showBarTransparentStyle();
        request(SZRetrofitManager.getInstance().getSZRequest().isPhoneCanBindV2(phone), new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                closeBar();
                if(isChangeMode()) {
                    startActivity(new BindPhoneConditionInfo(oldPhone, phone), getBindPhoneActivity());
                }else{
                    startActivity(new BindPhoneConditionInfo(phone), getBindPhoneActivity());
                }
                finish();
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                closeBar();
            }
        });
    }
    public abstract boolean isChangeMode();
    public abstract Class getBindPhoneActivity();

}
