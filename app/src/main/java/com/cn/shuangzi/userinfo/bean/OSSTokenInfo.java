package com.cn.shuangzi.userinfo.bean;

/**
 * Created by CN.
 */

public class OSSTokenInfo {
    private String filePath;
    private String baseURL;
    private String requestId;
    private String bucket;

    private Credentials credentials;
    class Credentials{
        private String accessKeyId;
        private String accessKeySecret;
        private String expiration;
        private String securityToken;
    }

    public String getAccessKeyId() {
        return credentials.accessKeyId;
    }

    public String getAccessKeySecret() {
        return credentials.accessKeySecret;
    }

    public String getSecurityToken() {
        return credentials.securityToken;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public String getBucket() {
        return bucket;
    }
}
