package com.cn.shuangzi.userinfo.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.userinfo.R;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.cn.shuangzi.view.ClearEditText;

import cn.yzl.countdown_library.CountDownButton;
import io.reactivex.Observable;

/**
 * Created by CN.
 */

public abstract class SZCancellationAccountPhoneActivity extends SZBaseActivity implements SZInterfaceActivity {
    public TextView txtPhone;
    public ClearEditText edtCode;
    public CountDownButton btnGetCode;
    public Button btnCancellation;
    public String phone;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_cancellation_account_phone;
    }

    @Override
    protected void onBindChildViews() {
        txtPhone = findViewById(R.id.txtPhone);
        edtCode = findViewById(R.id.edtCode);
        btnGetCode = findViewById(R.id.btnGetCode);
        btnCancellation = findViewById(R.id.btnCancellation);
    }

    @Override
    protected void onBindChildListeners() {
        btnGetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMsgCode();
            }
        });
        btnCancellation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        showBackImgLeft(getBackImgLeft());
        setTitleTxt("注销帐号");
        phone = getPhone();
        if(TextUtils.isEmpty(phone)){
            try {
                SZToast.error("信息有误，请重试！");
            } catch (Exception e) {
                e.printStackTrace();
            }
            finish();
            return;
        }
        txtPhone.setText("手机号：" + getHidePhoneStr());
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    private void sendMsgCode() {
        if (!SZValidatorUtil.isMobile(phone)) {
            SZToast.warning("手机号失效，请重试！");
            btnGetCode.changeState(CountDownButton.STATE_NORMAL);
            finish();
            return;
        }
        sendMsgCode(phone);
    }

    public void sendMsgCode(String phone) {
        request(SZRetrofitManager.getInstance().getSZRequest().getVerificationCodeV2(phone), new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                closeBar();
                SZToast.success(getString(com.cn.shuangzi.loginplugin.R.string.success_send_msg_code));
                btnGetCode.start();
                btnCancellation.setEnabled(true);
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                closeBar();
                btnGetCode.changeState(CountDownButton.STATE_NORMAL);
            }
        });
    }

    public void showCountDown() {
        btnGetCode.start();
        btnCancellation.setEnabled(true);
    }

    public void resetCountDown() {
        btnGetCode.changeState(CountDownButton.STATE_NORMAL);
    }

    public void submit() {
        if (!SZValidatorUtil.isMobile(phone)) {
            SZToast.warning("手机号失效，请重试！");
            finish();
            return;
        }
        String code = edtCode.getText().toString().trim();
        if (!SZValidatorUtil.isValidString(code)) {
            SZToast.warning("请输入验证码！");
            return;
        }
        btnCancellation.setEnabled(false);
        login(phone, code);
    }

    public void login(String phone, String code) {
        showBar();
        request(SZRetrofitManager.getInstance().getSZRequest().getTokenByVerificationCodeV2(phone, code), new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                btnCancellation.setEnabled(true);
                cancellationAccount(data);
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                btnCancellation.setEnabled(true);
                closeBar();
            }
        });
    }

    private void cancellationAccount(String ticket) {

        if(getCancellationAccountObservable(ticket)!=null) {
            request(getCancellationAccountObservable(ticket), new SZCommonResponseListener() {
                @Override
                public void onResponseSuccess(String data) {
                    closeBar();
                    startActivity(getCancellationAccountSuccessClass());
                    finish();
                }

                @Override
                public void onResponseError(int errorCode, String errorMsg) {
                    closeBar();
                }
            });
        }else{
            request(SZRetrofitManager.getInstance().getSZRequest().cancellationAccount(getUserId(),ticket), new SZCommonResponseListener() {
                @Override
                public void onResponseSuccess(String data) {
                    closeBar();
                    startActivity(getCancellationAccountSuccessClass());
                    finish();
                }

                @Override
                public void onResponseError(int errorCode, String errorMsg) {
                    closeBar();
                }
            });
        }
    }

    public abstract Class getCancellationAccountSuccessClass();

    public abstract Observable<String> getCancellationAccountObservable(String ticket);

    public abstract String getPhone();

    public abstract String getHidePhoneStr();

    public abstract String getUserId();

}
