package com.cn.shuangzi.userinfo.permission;

import android.Manifest;
import android.app.Activity;

import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.permission.RequestPermissionHelper;
import com.cn.shuangzi.permission.RequestPermissionViewP;
import com.cn.shuangzi.userinfo.R;
import com.cn.shuangzi.util.SZXmlUtil;


/**
 * 动态申请照相机权限
 * Created by CN on 2017-10-20.
 */

public class RequestCameraPermissionHelper extends RequestPermissionHelper {
    private static final String[] permissionArrays = {Manifest.permission.CAMERA};
    private static final int[] permissionInfo = {R.string.open_camera_permit};
    public RequestCameraPermissionHelper(Activity context, RequestPermissionViewP requestPermissionViewP) {
        this(context, requestPermissionViewP,isAlertOnPermissionReallyDeclined());
    }
    public RequestCameraPermissionHelper(Activity context, RequestPermissionViewP requestPermissionViewP, boolean isAlertOnPermissionReallyDeclined) {
        super(context, requestPermissionViewP, permissionArrays, permissionInfo,isAlertOnPermissionReallyDeclined);
    }
    private static boolean isAlertOnPermissionReallyDeclined(){
        SZXmlUtil szXmlUtil = new SZXmlUtil(SZConst.SETTING);
        boolean isAlertOnPermissionReallyDeclined = szXmlUtil.getBoolean("camera_permission");
        if(!isAlertOnPermissionReallyDeclined) {
            szXmlUtil.put("camera_permission", true);
        }
        return isAlertOnPermissionReallyDeclined;
    }
}
