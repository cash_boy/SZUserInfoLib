package com.cn.shuangzi.userinfo.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */
public class BindPhoneConditionInfo implements Serializable {
    private String oldPhone;
    private String newPhone;

    public BindPhoneConditionInfo(String oldPhone, String newPhone) {
        this.oldPhone = oldPhone;
        this.newPhone = newPhone;
    }

    public BindPhoneConditionInfo(String newPhone) {
        this.newPhone = newPhone;
    }

    public BindPhoneConditionInfo() {
    }

    public String getOldPhone() {
        return oldPhone;
    }

    public void setOldPhone(String oldPhone) {
        this.oldPhone = oldPhone;
    }

    public String getNewPhone() {
        return newPhone;
    }

    public void setNewPhone(String newPhone) {
        this.newPhone = newPhone;
    }

    @Override
    public String toString() {
        return "BindPhoneConditionInfo{" +
                "oldPhone='" + oldPhone + '\'' +
                ", newPhone='" + newPhone + '\'' +
                '}';
    }
}
