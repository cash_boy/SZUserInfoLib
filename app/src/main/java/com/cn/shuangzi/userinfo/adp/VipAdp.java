package com.cn.shuangzi.userinfo.adp;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.cn.shuangzi.adp.SZBaseAdp;
import com.cn.shuangzi.bean.VipPriceInfo;
import com.cn.shuangzi.userinfo.R;

import java.util.List;

/**
 * Created by CN.
 */

public class VipAdp extends SZBaseAdp<VipPriceInfo> {
    public VipAdp(@Nullable List<VipPriceInfo> data) {
        super(R.layout.adp_vip, data);
    }

    @Override
    public void convertView(BaseViewHolder helper, VipPriceInfo item) {
        if(item.isSpecialPrice()){
            helper.setVisible(R.id.txtSpecial,true);
        }else{
            helper.setGone(R.id.txtSpecial,true);
        }

        helper.setText(R.id.txtName, item.getFpSubject());
        helper.setText(R.id.txtMonthPrice, item.getFpOriginalPrice() + "元/月");
        helper.setText(R.id.txtPrice, item.getFpPrice());
        helper.setBackgroundResource(R.id.lltBg, item.isCheck() ? R.mipmap.bg_vip_item_c : R.mipmap.bg_vip_item_n);
    }

}
