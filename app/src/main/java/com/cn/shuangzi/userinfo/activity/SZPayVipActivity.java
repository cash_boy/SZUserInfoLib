package com.cn.shuangzi.userinfo.activity;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.SZBaseWebActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.bean.VipPriceInfo;
import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.loginplugin.common.socialized.ShareWeChat;
import com.cn.shuangzi.loginplugin.common.socialized.pay.PayManager;
import com.cn.shuangzi.loginplugin.common.socialized.pay.PayType;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.userinfo.R;
import com.cn.shuangzi.util.SZTextViewWriterUtil;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;

import java.util.UUID;

public abstract class SZPayVipActivity extends SZBaseActivity implements View.OnClickListener, SZInterfaceActivity {
    public TextView txtName;
    public TextView txtPrice;
    public TextView txtDesc;
    public Button btnPay;
    public ImageView imgCheckWechat;
    public ImageView imgCheckAlipay;
    public CheckBox chkBoxAgree;
    public String payType = SZConst.WXPAY;
    public VipPriceInfo vipPriceInfoCheck;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_pay;
    }

    @Override
    protected void onBindChildViews() {

        txtName = findViewById(R.id.txtName);
        txtPrice = findViewById(R.id.txtPrice);
        txtDesc = findViewById(R.id.txtDesc);
        btnPay = findViewById(R.id.btnPay);
        imgCheckWechat = findViewById(R.id.imgCheckWechat);
        imgCheckAlipay = findViewById(R.id.imgCheckAlipay);
        chkBoxAgree = findViewById(R.id.chkBoxAgree);
    }

    @Override
    protected void onBindChildListeners() {
        findViewById(R.id.lltContactService).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareWeChat.getInstance().openCompanyService(
                        "ww319db112ce746335", "https://work.weixin.qq.com/kfid/kfc539223cd934af350"
                );
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        showBackImgLeft(getBackImgLeft());
        setTitleTxt("支付订单");
        vipPriceInfoCheck = getSerializableExtra();
        if (vipPriceInfoCheck == null) {
            finish();
            return;
        }
        SZTextViewWriterUtil.writeValue(txtName, vipPriceInfoCheck.getFpSubject());
        SZTextViewWriterUtil.writeValue(txtPrice, vipPriceInfoCheck.getFpPrice());
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    private void submitOrder() {
        btnPay.setEnabled(false);
        showBar(true, false);
        String uuid = null;
        try {
            uuid = UUID.randomUUID().toString().replace("-", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        request(SZRetrofitManager.getInstance().getSZRequest().submitOrderV2(getUserId(), payType, uuid, vipPriceInfoCheck.getFpId(),getWeChatAppId()), new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                btnPay.setEnabled(true);
                new PayManager(getActivity(), SZConst.ALIPAY.equals(payType) ? PayType.ALIPAY : PayType.WECHAT, data, new PayManager.OnPayListener() {
                    @Override
                    public void onPaySuccess() {
                        closeBar();
                        try {
                            for (Activity activity : SZApp.getInstance().getStackActivity()) {
                                if (activity instanceof SZBuyVipActivity) {
                                    activity.finish();
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        startActivity(getSynchronizeVipClass());
                        finish();
                    }

                    @Override
                    public void onPayCancel() {
                        closeBar();
                        SZToast.warning("您取消了支付!");
                    }

                    @Override
                    public void onPayFailed() {
                        closeBar();
                        SZToast.error("支付失败,请重试!");
                    }
                }).toPay();
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                btnPay.setEnabled(true);
                closeBar();
            }
        });
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.btnPay) {
            if (getUserId() == null) {
                startActivity(getLoginClass());
            } else {
                if (chkBoxAgree.isChecked()) {
                    submitOrder();
                } else {
                    SZUtil.showAgreeProtocolAlert(this, "同意并购买", getString(R.string.vip_service_agreement_symbol),
                            getVipProtocolUrl(), getWebViewActivity(),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    chkBoxAgree.setChecked(true);
                                    submitOrder();
                                }
                            });
                }
            }
        } else if (viewId == R.id.rltWechatPay) {
            payType = SZConst.WXPAY;
            imgCheckWechat.setImageResource(R.mipmap.pay_icon_choose);
            imgCheckAlipay.setImageResource(R.mipmap.pay_icon_not);
        } else if (viewId == R.id.rltAlipay) {
            payType = SZConst.ALIPAY;
            imgCheckAlipay.setImageResource(R.mipmap.pay_icon_choose);
            imgCheckWechat.setImageResource(R.mipmap.pay_icon_not);
        }else if (viewId == R.id.txtVipProtocol) {
            startActivity(getVipProtocolUrl(),getWebViewActivity());
        }
    }


    public abstract String getUserId();

    public abstract String getWeChatAppId();

    public abstract Class<?> getLoginClass();

    public abstract Class<?> getSynchronizeVipClass();

    public abstract String getVipProtocolUrl();

    public abstract Class<? extends SZBaseWebActivity> getWebViewActivity();
}
