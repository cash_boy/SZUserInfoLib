package com.cn.shuangzi.userinfo.bean;

import com.cn.shuangzi.bean.VipInfo;
import com.cn.shuangzi.util.SZValidatorUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by CN.
 */

public class AccountConditionInfo implements Serializable{
    private String logicConsumerTripartite;
    private List<VipInfo> logicMemberList;
    public boolean isPhoneType(){
        return "PHONE".equalsIgnoreCase(logicConsumerTripartite);
    }
    public boolean isAlipayType(){
        return "ALIPAY".equalsIgnoreCase(logicConsumerTripartite);
    }
    public boolean isWeChatType(){
        return "WECHAT".equalsIgnoreCase(logicConsumerTripartite);
    }
    public boolean isWeChat2Type(){
        return "WECHAT_2".equalsIgnoreCase(logicConsumerTripartite);
    }

    public VipInfo getLogicMember() {
        if(SZValidatorUtil.isValidList(logicMemberList)) {
            return logicMemberList.get(0);
        }
        return null;
    }

    public String getLogicConsumerTripartite() {
        return logicConsumerTripartite;
    }
}
