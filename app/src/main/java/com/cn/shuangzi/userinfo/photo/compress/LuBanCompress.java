package com.cn.shuangzi.userinfo.photo.compress;

import android.content.Context;

import com.luck.picture.lib.entity.LocalMedia;

import org.devio.takephoto.compress.CompressConfig;
import org.devio.takephoto.model.LubanOptions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.shaohui.advancedluban.Luban;
import me.shaohui.advancedluban.OnCompressListener;
import me.shaohui.advancedluban.OnMultiCompressListener;

public class LuBanCompress implements org.devio.takephoto.compress.CompressImage {
    private List<LocalMedia> images;
    private CompressImage.CompressListener listener;
    private Context context;
    private LubanOptions options;
    private ArrayList<File> files = new ArrayList<>();
    private List<LocalMedia> noCompressMediaList;
    public LuBanCompress(Context context, CompressConfig config, List<LocalMedia> images, CompressImage.CompressListener listener) {
        options = config.getLubanOptions();
        this.images = images;
        this.listener = listener;
        this.context = context;
        noCompressMediaList = new ArrayList<>();
    }

    @Override
    public void compress() {
        if (images == null || images.isEmpty()) {
            listener.onCompressFailed(images, " images is null");
            return;
        }
        for (LocalMedia image : images) {
            if (image == null) {
                listener.onCompressFailed(images, " There are pictures of compress  is null.");
                return;
            }
            if(image.getRealPath() != null){
                files.add(new File(image.getRealPath()));
            }else{
                noCompressMediaList.add(image);
            }
        }
        if (images.size() == 1) {
            compressOne();
        } else {
            compressMulti();
        }
    }

    private void compressOne() {
        Luban.compress(context, files.get(0))
                .putGear(Luban.CUSTOM_GEAR)
                .setMaxHeight(options.getMaxHeight())
                .setMaxWidth(options.getMaxWidth())
                .setMaxSize(options.getMaxSize())
                .launch(new OnCompressListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(File file) {
                        LocalMedia image = images.get(0);
                        image.setCompressPath(file.getPath());
                        image.setCompressed(true);
                        listener.onCompressSuccess(images);
                    }

                    @Override
                    public void onError(Throwable e) {
                        listener.onCompressFailed(images, e.getMessage() + " is compress failures");
                    }
                });
    }

    private void compressMulti() {
        Luban.compress(context, files)
                .putGear(Luban.CUSTOM_GEAR)
                .setMaxSize(options.getMaxSize())                // limit the final image size（unit：Kb）
                .setMaxHeight(options.getMaxHeight())             // limit image height
                .setMaxWidth(options.getMaxWidth())
                .launch(new OnMultiCompressListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(List<File> fileList) {
                        handleCompressCallBack(fileList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        listener.onCompressFailed(images, e.getMessage() + " is compress failures");
                    }
                });
    }

    private void handleCompressCallBack(List<File> files) {
        for (int i = 0; i < images.size(); i++) {
            LocalMedia image = images.get(i);
            image.setCompressed(true);
            if(noCompressMediaList.contains(image)){
                continue;
            }
            image.setCompressPath(files.get(0).getPath());
            files.remove(0);
        }
        listener.onCompressSuccess(images);
    }
}
