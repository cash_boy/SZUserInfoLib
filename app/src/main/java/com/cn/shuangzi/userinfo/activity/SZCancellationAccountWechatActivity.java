package com.cn.shuangzi.userinfo.activity;

import android.view.View;
import android.widget.ImageView;

import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.loginplugin.common.socialized.ThirdLogin;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.userinfo.R;
import com.cn.shuangzi.userinfo.common.UserManager;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;

import io.reactivex.Observable;


/**
 * Created by CN.
 */

public abstract class SZCancellationAccountWechatActivity extends SZBaseActivity implements ThirdLogin.OnThirdLoginResponseListener, SZInterfaceActivity {
    public static ThirdLogin thirdLogin;
    public ImageView imgAppLogo;
    public String platform;
    private boolean isRequestingUserInfo;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_cancellation_account_wechat;
    }

    @Override
    protected void onBindChildViews() {
        imgAppLogo = findViewById(R.id.imgAppLogo);
    }

    @Override
    protected void onBindChildListeners() {
        findViewById(R.id.btnCancellation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        if(getAppLogo()!=0) {
            imgAppLogo.setImageResource(getAppLogo());
        }
        showBackImgLeft(getBackImgLeft());
        setTitleTxt("注销帐号");

        platform = getStringExtra();
        if(platform == null){
            finish();
        }
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    public void login() {
        if (SZUtil.isAppInstalled("com.tencent.mm")) {
            thirdLogin = new ThirdLogin(this, ThirdLogin.WE_CHAT_PLATFORM, this);
            thirdLogin.login();
        } else {
            SZToast.warning("您还没有安装微信!");
        }
    }

    @Override
    public void onSuccess(String thirdId) {
        if(!isRequestingUserInfo) {
            isRequestingUserInfo = true;
            showBar();
            request(SZRetrofitManager.getInstance().getSZRequest().loginThirdV2(platform, thirdId), new SZCommonResponseListener() {
                @Override
                public void onResponseSuccess(String data) {
                    cancellationAccount(data);
                }

                @Override
                public void onResponseError(int errorCode, String errorMsg) {
                    isRequestingUserInfo = false;
                    closeBar();
                }
            });
        }
    }

    private void cancellationAccount(String ticket) {
        Observable<String> cancellationAccountObservable = getCancellationAccountObservable(ticket);
        if(cancellationAccountObservable!=null) {
            request(cancellationAccountObservable, new SZCommonResponseListener() {
                @Override
                public void onResponseSuccess(String data) {
                    closeBar();
                    startActivity(getCancellationAccountSuccessClass());
                    finish();
                }

                @Override
                public void onResponseError(int errorCode, String errorMsg) {
                    isRequestingUserInfo = false;
                    closeBar();
                }
            });
        }else{
            request(SZRetrofitManager.getInstance().getSZRequest().cancellationAccount(getUserId(),ticket), new SZCommonResponseListener() {
                @Override
                public void onResponseSuccess(String data) {
                    closeBar();
                    startActivity(getCancellationAccountSuccessClass());
                    finish();
                }

                @Override
                public void onResponseError(int errorCode, String errorMsg) {
                    isRequestingUserInfo = false;
                    closeBar();
                }
            });
        }
    }
    @Override
    public void onFailed() {

    }

    @Override
    public void onCancel() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        thirdLogin = null;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public abstract Class getCancellationAccountSuccessClass();

    public abstract Observable<String> getCancellationAccountObservable(String ticket);

    public abstract int getAppLogo();

    public abstract String getUserId();
}

