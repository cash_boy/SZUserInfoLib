package com.cn.shuangzi.userinfo.permission;

import android.Manifest;
import android.app.Activity;

import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.permission.RequestPermissionHelper;
import com.cn.shuangzi.permission.RequestPermissionViewP;
import com.cn.shuangzi.userinfo.R;
import com.cn.shuangzi.util.SZXmlUtil;


/**
 * 动态申请照相机权限
 * Created by CN on 2017-10-20.
 */

public class RequestWritePermissionHelper extends RequestPermissionHelper {
    private static final String[] permissionArrays = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final int[] permissionInfo = {R.string.open_storage_permit};

    public RequestWritePermissionHelper(Activity context, RequestPermissionViewP requestPermissionViewP) {
        this(context, requestPermissionViewP,isAlertOnPermissionReallyDeclined());
    }

    public RequestWritePermissionHelper(Activity context, RequestPermissionViewP requestPermissionViewP, boolean isAlertOnPermissionReallyDeclined) {
        super(context, requestPermissionViewP, permissionArrays, permissionInfo,isAlertOnPermissionReallyDeclined);
    }

    public RequestWritePermissionHelper(Activity context,int[] permissionInfo,RequestPermissionViewP requestPermissionViewP) {
        super(context, requestPermissionViewP, permissionArrays, permissionInfo,isAlertOnPermissionReallyDeclined());
    }

    public RequestWritePermissionHelper(Activity context,int[] permissionInfo,RequestPermissionViewP requestPermissionViewP, boolean isAlertOnPermissionReallyDeclined) {
        super(context, requestPermissionViewP, permissionArrays, permissionInfo,isAlertOnPermissionReallyDeclined);
    }

    private static boolean isAlertOnPermissionReallyDeclined(){
        SZXmlUtil szXmlUtil = new SZXmlUtil(SZConst.SETTING);
        boolean isAlertOnPermissionReallyDeclined = szXmlUtil.getBoolean("write_permission");
        if(!isAlertOnPermissionReallyDeclined) {
            szXmlUtil.put("write_permission", true);
        }
        return isAlertOnPermissionReallyDeclined;
    }
}
