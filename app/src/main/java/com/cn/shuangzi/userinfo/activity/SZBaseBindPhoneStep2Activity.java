package com.cn.shuangzi.userinfo.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.common.SZCommonEvent;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.userinfo.R;
import com.cn.shuangzi.userinfo.bean.BindPhoneConditionInfo;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.cn.shuangzi.view.ClearEditText;
import com.cn.shuangzi.view.sweet_alert.SweetAlertDialog;

import org.greenrobot.eventbus.EventBus;

import cn.yzl.countdown_library.CountDownButton;
import io.reactivex.Observable;

/**
 * Created by mu on 2019/12/30.
 */

public abstract class SZBaseBindPhoneStep2Activity extends SZBaseActivity implements SZInterfaceActivity {
    public TextView txtPhone;
    public ClearEditText edtCode;
    public CountDownButton btnGetCode;
    public Button btnBind;
    public BindPhoneConditionInfo conditionInfo;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_bind_phone_step2;
    }

    @Override
    protected void onBindChildViews() {
        txtPhone = findViewById(R.id.txtPhone);
        edtCode = findViewById(R.id.edtCode);
        btnGetCode = findViewById(R.id.btnGetCode);
        btnBind = findViewById(R.id.btnBind);
    }

    @Override
    protected void onBindChildListeners() {
        btnGetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMsgCode();
            }
        });
        btnBind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        conditionInfo = getSerializableExtra();
        if (conditionInfo == null) {
            finish();
            return;
        }
        onChildViewCreatedPre();
        showBackImgLeft(getBackImgLeft());

        if (isChangeMode()) {
            setTitleTxt("更换手机");
        } else {
            setTitleTxt("绑定手机");
        }

        txtPhone.setText("手机号：" + SZUtil.getHidePhoneStr(conditionInfo.getNewPhone()));
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    private void sendMsgCode() {
        if (!SZValidatorUtil.isMobile(conditionInfo.getNewPhone())) {
            btnGetCode.changeState(CountDownButton.STATE_NORMAL);
            SZToast.error("手机号失效，请重试！");
            finish();
            return;
        }
        sendMsgCode(conditionInfo.getNewPhone());
    }

    public void sendMsgCode(String phone) {
        request(SZRetrofitManager.getInstance().getSZRequest().getVerificationCodeV2(phone), new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                closeBar();
                SZToast.success(getString(R.string.success_send_msg_code));
                btnGetCode.start();
                btnBind.setEnabled(true);
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                closeBar();
                btnGetCode.changeState(CountDownButton.STATE_NORMAL);
            }
        });
    }

    public void showCountDown() {
        btnGetCode.start();
        btnBind.setEnabled(true);
    }

    public void resetCountDown() {
        btnGetCode.changeState(CountDownButton.STATE_NORMAL);
    }

    public void submit() {
        String code = edtCode.getText().toString().trim();
        if (TextUtils.isEmpty(code)) {
            SZToast.warning("请输入验证码！");
            return;
        }
        btnBind.setEnabled(false);
        bind(code);
    }

    public void bind(String code) {
        showBarTransparentStyle();
        String oldPhone = conditionInfo.getOldPhone();
        String newPhone = conditionInfo.getNewPhone();
        if(isChangeMode()) {
            if (TextUtils.isEmpty(oldPhone) || TextUtils.isEmpty(newPhone)) {
                SZToast.error("手机号已失效，请重试！");
                finish();
                return;
            }
        }else{
            oldPhone = "";
            if (TextUtils.isEmpty(newPhone)) {
                SZToast.error("手机号已失效，请重试！");
                finish();
                return;
            }
        }
        request(SZRetrofitManager.getInstance().getSZRequest().updateBindingConsumerPhoneV2(
                getUserId(),oldPhone,newPhone
                ,code), new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                closeBar();
                btnBind.setEnabled(true);
                bindSuccess();
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                btnBind.setEnabled(true);
                closeBar();
            }
        });
    }
    public void bindSuccess(){
        onPhoneSaveSuccess(conditionInfo.getNewPhone());
        String title = isChangeMode()?"手机更换成功":"手机绑定成功";
        String content = isChangeMode()?"您可以使用新手机号了":"您的帐号安全等级提升，以后可以使用手机号登录了";
        showSuccessAlert(title, content, "知道了", new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                EventBus.getDefault().post(new SZCommonEvent(SZCommonEvent.BIND_PHONE_EVENT,conditionInfo.getNewPhone()));
                sweetAlertDialog.dismissWithAnimation();
                finish();
            }
        }).setCancelable(false);
    }

    public abstract boolean isChangeMode();
    public abstract String getUserId();
    public abstract void onPhoneSaveSuccess(String phone);

}
