package com.cn.shuangzi.userinfo.bean;

import com.cn.shuangzi.bean.VipInfo;
import com.cn.shuangzi.userinfo.common.UserManager;
import com.cn.shuangzi.util.SZValidatorUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by CN.
 */

public class AccountConditionInfo2 implements Serializable{
    private List<String> logicConsumerTripartite;
    private List<VipInfo> logicMemberList;

    public VipInfo getLogicMember() {
        if(SZValidatorUtil.isValidList(logicMemberList)) {
            return logicMemberList.get(0);
        }
        return null;
    }

    public List<String> getLogicConsumerTripartite() {
        return logicConsumerTripartite;
    }

    public boolean isPlatform(String platformCurrent){
        if(SZValidatorUtil.isValidString(platformCurrent)) {
            if (SZValidatorUtil.isValidList(logicConsumerTripartite)) {
                for (String platform : logicConsumerTripartite) {
                    if (platformCurrent.equals(platform)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isWeChat2Platform(){
        return isPlatform(UserManager.PLATFORM_WECHAT_2);
    }

    public boolean isWeChatPlatform(){
        return isPlatform(UserManager.PLATFORM_WECHAT);
    }

    public boolean isAlipayPlatform(){
        return isPlatform(UserManager.PLATFORM_ALIPAY);
    }

    public boolean isPhonePlatform(){
        return isPlatform(UserManager.PLATFORM_PHONE);
    }

}
