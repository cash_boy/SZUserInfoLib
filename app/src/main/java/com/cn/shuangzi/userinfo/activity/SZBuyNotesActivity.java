package com.cn.shuangzi.userinfo.activity;

import android.widget.TextView;

import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.userinfo.R;

/**
 * Created by CN.
 */

public abstract class SZBuyNotesActivity extends SZBaseActivity implements SZInterfaceActivity {
    public TextView txtNotes;
    @Override
    protected int onGetChildView() {
        return R.layout.activity_buy_notes;
    }

    @Override
    protected void onBindChildViews() {
        txtNotes = findViewById(R.id.txtNotes);
    }

    @Override
    protected void onBindChildListeners() {
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        showBackImgLeft(getBackImgLeft());
        setTitleTxt(R.string.txt_buy_notes);
        txtNotes.setText(R.string.txt_buy_notes_desc);
    }
    @Override
    protected void onReloadData(boolean isRefresh) {

    }
}
