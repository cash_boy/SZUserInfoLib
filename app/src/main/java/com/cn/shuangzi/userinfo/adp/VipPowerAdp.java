package com.cn.shuangzi.userinfo.adp;

import android.content.Context;
import androidx.annotation.Nullable;
import android.widget.ImageView;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.cn.shuangzi.adp.SZBaseAdp;
import com.cn.shuangzi.userinfo.R;
import com.cn.shuangzi.userinfo.bean.VipPowerInfo;
import com.cn.shuangzi.util.SZImageLoader;

import java.util.List;

/**
 * Created by CN.
 */

public class VipPowerAdp extends SZBaseAdp<VipPowerInfo> {
    private SZImageLoader imageLoader;
    public VipPowerAdp(Context context, @Nullable List<VipPowerInfo> data) {
        super(context, R.layout.adp_vip_power, data);
        imageLoader = new SZImageLoader(context);
    }

    @Override
    public void convertView(BaseViewHolder helper, VipPowerInfo item) {
        helper.setText(R.id.txtPowerName,item.getPowerName());
        if(item.isUrlMode()){
            imageLoader.load((ImageView)helper.getView(R.id.imgPower),item.getImgUrl());
        }else{
            imageLoader.load((ImageView)helper.getView(R.id.imgPower),item.getImgRes());
        }
    }
}
