package com.cn.shuangzi.userinfo.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */
public class PictureInfo implements Serializable {
    private String compressUrl;
    private String originalUrl;
    private int width;
    private int height;

    public PictureInfo() {
    }

    public PictureInfo(String compressUrl) {
        this.compressUrl = compressUrl;
    }

    public PictureInfo(String compressUrl, int width, int height) {
        this.compressUrl = compressUrl;
        this.width = width;
        this.height = height;
    }

    public PictureInfo(String compressUrl, String originalUrl, int width, int height) {
        this.compressUrl = compressUrl;
        this.originalUrl = originalUrl;
        this.width = width;
        this.height = height;
    }

    public String getCompressUrl() {
        return compressUrl;
    }

    public void setCompressUrl(String compressUrl) {
        this.compressUrl = compressUrl;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    @Override
    public String toString() {
        return "PictureInfo{" +
                "compressUrl='" + compressUrl + '\'' +
                ", originalUrl='" + originalUrl + '\'' +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
